

function previewImage() {
    const file = document.getElementById("file").files[0];
    const preview = document.getElementById("imagePreview");

    if (file) {
        const reader = new FileReader();

        reader.onload = function (event) {
            preview.src = event.target.result;
            preview.style.display = "block"; // Affiche l'aperçu de l'image
        }

        reader.readAsDataURL(file);
    } else {
        preview.style.display = "none"; // Cache l'aperçu si aucun fichier n'est sélectionné
    }
}


$(document).ready(function () {
    $('#upload-form').submit(function (event) {
        event.preventDefault(); // Empêcher la soumission par défaut

        // Collectez les données du formulaire, par exemple le fichier uploadé
        const formData = new FormData(this);

        // Exécutez votre action AJAX pour envoyer les données
        $.ajax({
            type: "POST",
            url: "/", // Mettez l'URL correcte pour le traitement de votre formulaire
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                // Mettre à jour les réponses en temps réel pour chaque serveur
                for (var i = 0; i < 4; i++) {
                    updateResponse(i);
                }
            },
            error: function () {
                // Gérer les erreurs de l'envoi AJAX (si nécessaire)
            }
        });
    });

    function updateResponse(index) {
        $.ajax({
            type: "GET",
            url: `/get_response/${index}`,  // Vous devrez créer une route pour cela dans Flask
            success: function (data) {
                $("#response" + index).html(data);
                if (index == 0){
                    $(`#td0`).css({
                        "background-image": 'url(' + img1 + ')',
                        "background-size": "cover",
                        "background-position": "center"
                    });
                    $(`#0`).css({
                        "display": "none",
                    });
                }
                else if (index == 1) {
                    $(`#td1`).css({
                        "background-image": 'url(' + img2 + ')',
                        "background-size": "cover",
                        "background-position": "center"
                    });
                    $(`#1`).css({
                        "display": "none",
                    });
                } else if (index == 2) {
                    $(`#td2`).css({
                        "background-image": 'url(' + img3 + ')',
                        "background-size": "cover",
                        "background-position": "center"
                    });
                    $(`#2`).css({
                        "display": "none",
                    });
                } else if (index == 3) {
                    $(`#td3`).css({
                        "background-image": 'url(' + img4 + ')',
                        "background-size": "cover",
                        "background-position": "center"
                    });
                    $(`#3`).css({
                        "display": "none",
                    });
                }

            },
            error: function () {
                // Gérer les erreurs
                $("#response" + index).html("Erreur lors de la récupération de la réponse.");
            }
        });
    }

});