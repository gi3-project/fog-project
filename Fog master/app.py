from flask import Flask, render_template, request, send_from_directory
import os
import requests
import cv2
import json
import threading


app = Flask(__name__)
app.static_folder = 'assets'


UPLOAD_FOLDER = 'assets/uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Initialisez la liste de statuts
result_status = [False] * 4
result_list = [None] * 4  # Initialisation avec une liste vide
partition_images = []  # Liste pour stocker les partitions converties en base64


@app.route('/get_response/<int:index>', methods=['GET'])
def get_response(index):
    if result_status[index]:
        return result_list[index]["classification"]
    else:
        return "En attente de la réponse du serveur " + str(index + 1)


def send_and_store(server, partition, idx):
    cropped_img_filename = f"temp_part_{idx}.png"
    cv2.imwrite(cropped_img_filename, partition)

    with open(cropped_img_filename, "rb") as cropped_file:
        response = requests.post(server, files={"file": cropped_file})
        response_dict = json.loads(response.text)
        id = response_dict["id"] - 1
        result_list[id] = response_dict
        result_status[id] = True

        print(f"Response from {server}: {response.text}")




@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files.get('file')
        if not file:
            return 'No file part', 400
        if file.filename == '':
            return 'No selected file', 400
        if file:
            image = file.filename
            filename = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
            file.save(filename)

            # Appeler la nouvelle méthode pour le traitement
            process_uploaded_file(filename)

        # return 'File uploaded and forwarded to other servers.'

    return render_template('upload.html')


def process_uploaded_file(filename):
    # Lire l'image avec cv2
    img = cv2.imread(filename)

    height, width = img.shape[:2]
    half_height, half_width = height // 2, width // 2

    # Découpage des parties en fonction de vos instructions
    partitions = [
        img[:half_height, :half_width],  # Moitié supérieure gauche
        img[:half_height, half_width:],  # Moitié supérieure droite
        img[half_height:, :half_width],  # Moitié inférieure gauche
        img[half_height:, half_width:]   # Moitié inférieure droite
    ]

    for idx, partition in enumerate(partitions):
        try:
            partition_filename = f"{idx}.png"  # Utilisez simplement l'index pour le nom du fichier
            partition_path = os.path.join(app.config["UPLOAD_FOLDER"], partition_filename)
            cv2.imwrite(partition_path, partition)
        except Exception as e:
            raise e

    # Liste des serveurs
    servers = [
        "http://192.168.1.49:5001/upload",
        "http://192.168.1.49:5002/upload",
        "http://192.168.1.49:5003/upload",
        "http://192.168.1.49:5004/upload"
    ]

    # Utiliser des threads pour envoyer les partitions aux serveurs en parallèle
    threads = []
    for idx, (partition, server) in enumerate(zip(partitions, servers)):
        thread = threading.Thread(target=send_and_store, args=(server, partition, idx))
        thread.start()
        threads.append(thread)

    # Attendre que tous les threads aient terminé
    for thread in threads:
        thread.join()

    # Supprimer les fichiers temporaires après que tous les threads aient terminé
    for idx in range(len(partitions)):
        cropped_img_filename = f"temp_part_{idx}.png"
        os.remove(cropped_img_filename)

if __name__ == '__main__':
    app.run()